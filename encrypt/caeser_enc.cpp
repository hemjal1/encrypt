#include "caeser_enc.hh"
#include "QDebug"


Caeser_enc::Caeser_enc()
{

}

std::string Caeser_enc::get_encrypted_text(std::string key, std::string text)
{
    _encryption_key = key;
    _target_text = text;
    _do_caeser_enc();
    return _output_encrypted_text;
}

void Caeser_enc::_do_caeser_enc()
{
    qDebug()<<"Doing encyption";
    int str_size= _encryption_key.size();
    if(26!= str_size)
    {
        qDebug() << ERR_LIMITED_CHAR ;
        _output_encrypted_text = ERR_LIMITED_CHAR;
        return;
    }
    for(int i=0; i<26;i++)
    {
        char ch = _encryption_key[i];
        if(!(ch <= 'z' && ch >= 'a'))
        {
            qDebug() << ERR_LOWER_CASE_CHAR ;
            _output_encrypted_text = ERR_LOWER_CASE_CHAR;
            return;
        }
    }
    char letter, chk_char='a';
    for(int i=0; i<26;i++)
    {
        letter = _encryption_key.find(chk_char);
        if(chk_char != _encryption_key[letter]){
            qDebug() << ERR_NOT_ALL_CHAR ;
            _output_encrypted_text = ERR_NOT_ALL_CHAR;
            return;
        } else {
            chk_char++;
        }
    }

    int text_len = _target_text.size();
    for(int i=0; i<text_len;i++)
    {
        char ch = _target_text[i];
        if(!(ch <= 'z' && ch >= 'a'))
        {
            qDebug() << ERR_LOWER_CASE_CHAR ;
            _output_encrypted_text = ERR_LOWER_CASE_CHAR;
            return ;
        }
        else {
            int pos= ch-97;
            _output_encrypted_text += _encryption_key[pos];
        }
    }
    _output_encrypted_text [text_len]='\0';
}

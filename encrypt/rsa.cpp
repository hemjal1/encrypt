#include "rsa.hh"

rsa::rsa()
{

}

// we will run the function only once to fill the set of prime numbers
// method used to fill the primes set is seive of eratosthenes
void rsa::primefiller()
{
    std::vector<bool> seive(250, true);
    seive[0] = false;
    seive[1] = false;
    for (int i = 2; i < 250; i++) {
        for (int j = i * 2; j < 250; j += i) {
            seive[j] = false;
        }
    } // filling the prime numbers
    for (size_t i = 0; i < seive.size(); i++) {
        if (seive[i])
            prime.insert(i);
    }
}
// picking a random prime number and erasing that prime
// number from list because p!=q
int rsa::pickrandomprime()
{
    int k = rand() % prime.size();
    auto it = prime.begin();
    while (k--)
        it++;
    int ret = *it;
    prime.erase(it);
    return ret;
}
void rsa::setkeys()
{
    int prime1 = pickrandomprime(); // first prime number
    int prime2 = pickrandomprime(); // second prime number
    n = prime1 * prime2;
    int fi = (prime1 - 1) * (prime2 - 1);
    int e = 2;
    while (1) {
        if (std::__gcd(e, fi) == 1)
            break;
        e++;
    } // d = (k*Φ(n) + 1) / e for some integer k
    public_key = e;
    int d = 2;
    while (1) {
        if ((d * e) % fi == 1)
            break;
        d++;
    }
    private_key = d;
}

// Encrypt the given number/text
long long int rsa::encrypt(double message)
{
    int e = public_key;
    long long int encrpyted_text = 1;
    while (e--) {
        encrpyted_text *= message;
        encrpyted_text %= n;
    }
    return encrpyted_text;
}
// Decrypt the given number/text
long long int rsa::decrypt(int encrpyted_text)
{
    int d = private_key;
    long long int decrypted = 1;
    while (d--) {
        decrypted *= encrpyted_text;
        decrypted %= n;
    }
    return decrypted;
}
// first converting each character to its ASCII value and
// then encoding it then decoding the number to get the
// ASCII and converting it to character
std::string rsa::encoder(std::string message)
{
    primefiller();
    setkeys();
    std::vector<int> form;
    std::string output_encrypted_data;
    for (auto& letter : message)
        form.push_back(encrypt((int)letter));  // encrypting using encoding function
    for (auto element : form) {
        output_encrypted_data.append(std::to_string(element));
    }
    return output_encrypted_data;
}
std::string rsa::decoder(std::vector<int> encoded)
{
    std::string s;
    for (auto& num : encoded)
        s += decrypt(num);         // decrypting using decoding function
    return s;
}

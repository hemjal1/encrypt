#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include "QDebug"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->pushButton_encrypt,&QPushButton::clicked,this,&MainWindow::get_encrypted_text);
    connect(ui->pushButton_clear_all,&QPushButton::clicked,this,&MainWindow::clear_inputs);
    connect(ui->pushButton_copy_enc_txt,&QPushButton::clicked,this,&MainWindow::copy_encrypted_text);
}


void MainWindow::get_encrypted_text()
{
    QString enc_method = ui->comboBox->currentText();
    QString user_encryption_key = ui->textEdit_enc_key->toPlainText();
    string user_encyption_key_str = user_encryption_key.toStdString();
    qDebug()<<user_encryption_key;
    QString target_text = ui->textEdit_target_txt->toPlainText();
    string user_target_text = target_text.toStdString();
    clear_outputs();
    if(enc_method == "Caeser Encryption") {
        qDebug() << enc_method;
        string output_encrypted_text = caeser_encryption.get_encrypted_text(user_encyption_key_str,user_target_text);
        ui->textBrowser_encrypted_txt->setText(QString::fromStdString(output_encrypted_text));
    }
    else if(enc_method == "Symmetric Key Encryption") {
        qDebug() << enc_method;
        string output_encrypted_text = symmetric_key_encryption.encrypt(user_target_text);
        std::cout<<output_encrypted_text.size()<<std::endl;
        for(size_t i=0; i<output_encrypted_text.size(); i++){
            std::cout<<  (unsigned int)output_encrypted_text[i];
        }
        ui->textBrowser_encrypted_txt->setText(QString::fromStdString(output_encrypted_text));
    }
    else if(enc_method == "RSA Algorithm") {
        qDebug() << enc_method;
        string output_encrypted_text = rsa_encryption.encoder(user_target_text);
        ui->textBrowser_encrypted_txt->setText(QString::fromStdString(output_encrypted_text));
    }
}


void MainWindow::clear_inputs()
{
    ui->textEdit_enc_key->clear();
    ui->textEdit_target_txt->clear();
    ui->textBrowser_encrypted_txt->clear();
}

void MainWindow::clear_outputs()
{
    ui->textBrowser_encrypted_txt->clear();
}

void MainWindow::copy_encrypted_text()
{
    //ui->textBrowser_encrypted_txt->copyAvailable(true);
    ui->textBrowser_encrypted_txt->copy();
}

MainWindow::~MainWindow()
{
    delete ui;
}



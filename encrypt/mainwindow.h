#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "caeser_enc.hh"
#include "sk_enc.hh"
#include "rsa.hh"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void get_encrypted_text(void);
    void get_decrypted_text(void);
    void clear_inputs(void);
    void clear_outputs(void);
    void copy_encrypted_text(void);

    Caeser_enc caeser_encryption;
    SK_ENC symmetric_key_encryption;
    rsa rsa_encryption;

};
#endif // MAINWINDOW_H

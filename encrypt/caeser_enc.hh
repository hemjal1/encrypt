#ifndef CAESER_ENC_HH
#define CAESER_ENC_HH
#include <string>

using namespace std;

#define ERR_LIMITED_CHAR "Error! The encryption key must contain 26 characters."
#define ERR_LOWER_CASE_CHAR "Error! The encryption key must contain only lower case characters."
#define ERR_NOT_ALL_CHAR "Error! The encryption key must contain all alphabets a-z."

class Caeser_enc
{
public:
    Caeser_enc();

    std::string get_encrypted_text(std::string key, std::string text);

private:
    string _encryption_key;
    string _target_text;
    string _output_encrypted_text = "";
    void _do_caeser_enc(void);
};

#endif // CAESER_ENC_HH

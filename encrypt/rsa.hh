#ifndef RSA_HH
#define RSA_HH

#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<vector>
#include<set>
#include <algorithm>

class rsa
{
public:
    rsa();
    std::string encoder(std::string message);
    std::string decoder(std::vector<int> encoded);
private:
    std::set<int> prime; // a collection of prime numbers
    int public_key;
    int private_key;
    int n;
    void primefiller();
    int pickrandomprime();
    void setkeys();
    long long int encrypt(double message);
    long long int decrypt(int encrpyted_text);
};

#endif // RSA_HH

